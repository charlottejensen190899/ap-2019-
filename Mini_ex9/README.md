

/* If you want to see the whole mini-ex9 it can be found @ https://gitlab.com/Niller/ap2019/tree/master/Miniex9
   
   The source code to the mini-ex9 can be seen in my folder as well */  


![ScreenShot](Miniex9_Skærmbillede1.png)


URL: https://cdn.staticaly.com/gl/Niller/ap2019/raw/master/Miniex9/empty-example/index.html 


This mini exercise has been created in collaboration with Niels and Nicolai. 

We have chosen to call our program “Sexual content in the API?”, because through 
our process of creating this miniex, amazingly we found out that the Google API 
search machine is censoring sexual content and trying to match a non-sexual
result similar to the originally keyword. 

The program is about showing the difference between what comes up in an Google 
API search and in a normal Google search when searching for the word “dick”. We 
have used the official Google API called “Custom Search API”  in our program,
because we wanted to become more familiar with it and build upon the knowledge 
about the API that we gained from tuesday’s class. 

In this mini exercise we have chosen to work with the censorship of sexual content
in the Google API. The idea of working with sexual content came to use when we were
tinkering with Winnie’s code and exploring different types of keywords to search
for. It was through this tinkering of the code that we became aware of how the 
Google API systematically censured the output from keywords that had a reference
to sexual content. We found out that the API when confronted with a sexual keyword
either did not give us a picture at all or showed a picture that had no association
to something sexual. When we tried out the keyword “dick”, which we centered our 
program around, the API responded with a picture of a group of girls who were 
holding each others hands in what appeared to be a changing room, which did not 
correspond to the keyword we searched for. We understood this picture as a way 
for the Google API to avoid showing sexual content on its platform. Because of 
this, we wanted to create a program that contained to keyword “dick”.  

In order to get data sent from the Google API we had to 1)  register an API key 
within the chosen API, 2) get a engine ID and 3) specify what we wanted to query
and through these make a request. This communication between a client who sent a
request to the server, which in return respond on the client’s request can 
according to Eric Snodgrass and Winnie Soon be understood as a two-way 
communication process. This signify that the client and the server has to work
together to have the request showing up in the program. Likewise, in our program
we had to specify want we wanted to query along with an API key and an engine ID
before we even could make a request that eventually would be shown in the program.
However, in the last part of our process we experienced that the picture from the
API did not show when running the program. This was due to us exceeding the maximum
of  100 pictures per day, which is one of the terms one agree on when using the 
Google API.

Such an exchange of data contributes to a asymmetrical power-relationship where 
the API has the upper hand. This should be understood in the way that the Google
API in terms of its policy has the control of how their data can be used, for what
purposes it can be used and who can access it, which as a result can have a 
negative impact on those who are dependent on the API like us. Perhaps that is why
Eric Snodgrass and Winnie Soon in their article “API practices and paradigms: 
exploring the proctological parameters of APIs as key facilitators of sociotechnical
forms of exchange” emphasizes the need for further thinking through how APIs as a 
genre of networked software design might be best practiced within burgeoning and 
complex API-facilitated eco-systems. This need for further thinking, which is stated
by Snodgrass and Soon suggests that APIs like the Google API have a significance in
digital culture because of the increasingly prevalent practice of  sharing, 
automating, circulating and redistributing data across platforms. 

With the API working, we were wondering how we should represent the data that was
provided by the API and how we best could show the discovery of the censorship of 
sexual content in the Google API without offending the users of the program to much.
We put a lot of thought into which picture of a dick we should choose from the 
Google search to be displayed in the program. We ended up choosing a more cartoonish
version of a dick. The reason for this was that we did not want to take the risk of
choosing a more realistic version of a dick, because we were aware of how such a 
dick could be the center of a discussion in society concerning the use of censorship
and might seem a bit too provocative for some. However, if we had chosen to go with
a more realistic version of a dick, we could have emphasized the issue of censorship
even more. Instead of this we chose to include a question which should be used to
create an awareness of what control APIs have on what is being accessible to the 
user and what is not. 

If we should formulate a question in relation to web APIs or querying processes,
we would investigate how we could solve the key issues at stake in API practice,
which is argued for in the article “API practices and paradigms: exploring the 
proctological parameters of APIs as key facilitators of sociotechnical forms of 
exchange”. 
