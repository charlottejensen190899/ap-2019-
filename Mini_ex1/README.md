![ScreenShot](billede.png)


URL:
https://cdn.staticaly.com/gl/charlottejensen190899/ap-2019-/raw/master/Mini_ex1/index.html

Fixed URL: https://cdn.staticaly.com/gl/charlottejensen190899/ap-2019-/raw/master/Mini_ex1/index.html

**Think about My First Program**

My coding process can be described with the words  ‘systematic’, ’experimental' and ‘explorative’. 
This should be understood in the sense that I systematically read through the references in order 
to understand the syntaxes and then use them in practice and explore its possibilities in my 
program. When I was reading through some of the references, I began to feel like I was about to 
learn a new language by exploring the different syntaxes in the P5.js library. I chose to look more
into the reference textAlign() and study its syntax in order to get a more in depth understanding 
of what it means and what role it plays in the program when used. I mostly used the examples within
the page of the reference textAlign() to get an understanding of how textAlign() can 
be used in a program. In order to explore and experiment with this reference I used the 
following example as a foundation for my work and developed it further: 

>
	textSize(16);
	textAlign(RIGHT);
	text('ABCD', 50, 30);
	textAlign(CENTER);
	text('EFGH', 50, 50);
	textAlign(LEFT);
	text('IJKL', 50, 70);
>

As I began modifying the code by changing the different parameters with different numbers and texts,
I wondered what expression I could give the code if I added some color to it. I, then, went to the 
P5.js page to find the references for color to give my program another expression that did not involve
a white background with some black letters on. With the knowledge of how the color syntaxes worked, 
I began to work with the transparency of the words to give the letters a faded appearance. The idea 
was to illustrate that sweet words easily can fade into the background through the use of code. 

In my oppinion my coding process can be compared with learning a new language in school like English.
At first one has to know the grammar and syntaxes of the language in order to create an independent work 
with it. However, like learning the language English it is possible to learn it by exploring the different
syntaxes without having a in depth understanding of how it works. As long as one has an idea of what 
structures to follow. In my process I tried to prioritize to get an understanding of the different 
syntaxes before I used them in my program. The reason for this is that I believe that the outcome of a
program becomes better and more controlled if one knows exactly what the different parts of the syntaxes 
in the code does and how they work. In this way one can start exploring and modifying the code in a 
way that matches ones ideas for a program. In my case I started out with a vague idea of what I wanted 
to do with coding, but as I read through the syntaxes I got an idea that I could carry out with the 
knowledge I got from reading. It, then, became easier for me to write the code myself. 

I would say that coding, like reading and writing, gives one an individual empowerment in the sense that
it gives one access to more avenues of control and creativity. According to Anette Vee this individual 
empowerment is the most dominant current motivation for coding literacy. I agree with Vee on this point.
Code and coding practice for me is a way of exploring different design options in away that was not
possible before. The code itself becomes a new material to work with that gives new possibilities for 
personal expression and information generation, which contribute to an individual empowerment. In this 
sense I can agree with Vee on the fact that coding should be for everyone. However,it is written in a 
*New York Times* editorial that *“if coding is the new lingua franca, literacy rates for girl are dropping.”*
This could indicate that coding is not for everyone and when stating that coding is a literacy some might
be excluded. Likewise in reading and writing some might be excluded due to reading difficulties or the 
lack of skill to read and write in general. Is it then okay to declare coding as a literacy if it means
that some might be excluded and others might be included? And who should declare it a literacy? According to
Vee we should carefully consider what impact the ideological frames of literacy have on the way we think 
about programming and coding as well as the way we think about contemporary literacy. To Edward Stevens 
literacy is not a neutral technology and is used as a tool for individual and social transformation governed
by a purpose. It is for that reason important to be aware of how programming and coding are talked about 
and what effect it has on society at large. 
