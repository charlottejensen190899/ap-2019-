function setup() {
  createCanvas(650, 480);
  background('#222222')
  textSize(36);
  textFont('american typewriter');
  textStyle(NORMAL);
  textAlign(CENTER);
  fill(255,255);
  text('...Ni Wikipedia', 320, 200);
  fill(255,155); // fill text
  textAlign(CENTER);
  text('podría definir', 320, 240);
  fill(255,51); // fill text
  textAlign(CENTER);
  text('lo que siento por ti...', 320, 280)
  textSize(70);
  textFont('american typewriter');
  fill(0,80); // fill text
  textStyle(ITALIC);
  textAlign(RIGHT);
  text('"', 130, 190);
  textAlign(LEFT);
  text('"', 490, 350);
  textSize(70);
  textFont('american typewriter');
  textStyle(ITALIC);
  textAlign(RIGHT);
  text('__________', 506, 150);
  textAlign(LEFT);
  text('__________', 135, 299);
}

function draw() {

}
