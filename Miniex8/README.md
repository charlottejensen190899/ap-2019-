![ScreenShot](56306586_589071158261713_825918959730556928_n.png)


URL:
https://cdn.staticaly.com/gl/charlottejensen190899/ap-2019-/raw/master/Miniex8/empty-example/index.html?fbclid=IwAR3cxrWzf3ndY2D_YwJ-G_qGFHy-aa51ZY5NCLFg5t88x4SjzUPyZ7YDVfQ


I have been working on this mini exercise in collaboration with Niels and Nicolai
from my study group. As part of the collaborative process we decided that we only
would use one computer and together figuring out how to create the idea we have
agreed on. In the beginning we wanted to create an arrow, which consisted of the
alphabet from a to z in morse code. The reason for this was that we wanted to
work with written words beyond the latin alphabet. However, in the early process 
of making the program we came across technical problems primarily related to JSON. 
Because of the technical problems we decided to seek help in DD lab where we 
figured out what the problem was and how we could correct it. We found out that 
the problem we had, was due to us referring to the wrong ‘key’ within the source
code. With the problem fixed we could experiment with the visual expression and 
the naming of the variables in the source code in order to create a relationship
between the visual expression and the source code.  As we began experimenting 
with our program, our idea began to take shape and eventually ended up with us 
wanting to give a voice to those people, who where stranded somewhere where an 
ordinary use of languages was not possible. 

In the chapter “*Vocable code*” Geoff Cox and Alex McLean argue that the voice 
cannot be fully captured by computation, but instead this fact provides 
inspiration for new  ways of working with code and examining “code acts” to 
reveal other possibilities and motivations for working with speech and writing 
within the practice of coding. Seen from this perspective our program one could 
argue that our program could be seen as one way of working with code in relation
to speech and writing. Within the chapter called "*Vocable Code*" Cox and McLean
mention some examples, including the sound poem “*Ursonate*” of how code and
language can be used to create e-literature. According to Wikipedia e-literature
is a genre of literature, which encompasses works created exclusively on and for
electronic devices. Additionally e-literature can be defined as “a construction
whose literacy aesthetics emerge from computation, meaning that it cannot exist 
outside the digital space in which it was created. This is also the case with our
program, which aesthetics can be said to emerge from both our source code and the
interface.     
 
As mentioned before Cox and McLean list multiple examples of how code 
and languages can be used to create work within the genre of e-literature, which 
in some ways differ and are similar to our program. One of the examples in the 
chapter is the sound poem “*Ursonate*”, which is composed of non-lexical vocables 
that is strings of phonemes that make “non-words”. Unlike the sound poem 
“*Ursonate*”, our program does not explore the direct and perhaps synthetic 
relationship between mouth shape and sound generated by. Instead our program 
explores the relationship between code and written language, which is a different
way of working with code. To understand the relationship between code and written
language in our program, one have to see the interface and the source code as a
whole, because it else would not make sense. The meaning production in our case 
is produced by the programmer, who according to Geoff Cox and Alex McLean
*“[…] bring bodily meaning to their work by applying models of human perception,
and by trying to account for the ways that social bodies are drawn into the 
process of meaning production”* (Cox, Mclean, 26). 

According to Geoff Cox and Alex McLean all languages consist of *“closed systems 
of discrete semiotic elements, and the meaning is organized out of differences 
between elements that are meaningless in themselves”* (Cox, McLean, 19) and this
is also the case with our program. In our program we have used a closed system of
semiotic elements in the form of morse code that as individual signs do not make
sense, but as a whole contribute to the meaning production carried out by the 
programmer . In a way one could say that the programmer has the responsibility to
make a program that can be understood by others by either naming the variables in
a way that makes sense logically or by using secondary notation. In our program 
we have chosen to use variable names that does not represent what the code does,
but instead to the idea of a person being stranded somewhere where an ordinary
use of languages is not possible. This, however makes it more difficult for the
human reader to get an understanding of how the program works.  If the program's
interface and source code are looked upon isolated, the aesthetic aspect of the
program would be lost, because the program else would not make sense due to the
meaning production we as programmers have created in the program. 


**Bibliography**

Cox, Geoff and McLean, Alex. Speaking Code. MIT Press, 2013. 17-38. (ch.1 Vocable Code)

Wikipedia. Electronic literature. 2019. In https://en.wikipedia.org/wiki/Electronic_literature (last visited on 1/5 2019)
