
/* Prepared by Charlotte Jensen, Nicolai Ahle Bjerg and Niels Jacob Tobler
inspiration: http://www.generative-gestaltung.de/2/
*/

'use strict'; //all code in the script will execute in strict mode

var maxCount = 5000; // max count of the rectangles
var currentCount = 1;
var x = [];
var y = [];
var r = [];
var randomColor;
var img;

function preload(){
img = loadImage("Miniex7_p5_empty-example_kort.jpg")
}

function setup() {
  textFont("impact",)
  imageMode(CORNER);
  createCanvas(1400, 800);
  strokeWeight(1);
  frameRate(40);

  // first rectangle
  // rule #1: Set the first rectangle in Africa
  x[0] = width / 1.8;
  y[0] = height / 2;
  r[0] = 10;
}

function draw() {
  clear();
  image (img, 0,0,1400,800);

  textSize(40);
  text("SURVEILLANCE",620,80) //headline

  // frame 
  push();
  noFill();
  strokeWeight(5)
  rect(40,10,1345,730)
  pop();

  // create a random set of parameters
  // Rule #2: Generate a random size, x-position and y-position for a new rectangle
  var newR = random(3, 7);
  var newX = random(newR, width - newR);
  var newY = random(newR, height - newR);

  var closestDist = Number.MAX_VALUE;
  var closestIndex = 10;

  // which rectangle is the closest?
  //Rule #3: check which rectangle is the closest to the current rectangle
  for (var i = 0; i < currentCount; i++) {
    var newDist = dist(newX, newY, x[i], y[i]);
    if (newDist < closestDist) {
      closestDist = newDist;
      closestIndex = i;
    }
  }

  // show original position of the rectangle and a line to the new position
  // Rule #4: Show original position in the shape of a circle and create a line
  // that represent the angle to the new position
  fill(20);
  ellipse(newX, newY, newR * 2, newR * 2);
  line(newX, newY, x[closestIndex], y[closestIndex]);

  // aline it to the closest rectangle outline
  // Rule #5: Aline the new position to the closest rectangle outline
  var angle = atan2(newY - y[closestIndex], newX - x[closestIndex]);

  x[currentCount] = x[closestIndex] + cos(angle) * (r[closestIndex] + newR);
  y[currentCount] = y[closestIndex] + sin(angle) * (r[closestIndex] + newR);
  r[currentCount] = newR;
  currentCount++;

  // draw them in random colors within the green spectrum for each loop
  //Rule #6: Give the rectangles a random color within the green spectrum
  push();
  for (var i = 0; i < currentCount; i++) {
  randomColor = color(random(0),random(255),random(0));
	fill(randomColor);
    rect(x[i], y[i], r[i] * 2, r[i] * 2);
  }
  pop();

// Rule #7: Repeat until the max count of rectangles is reached
  if (currentCount >= maxCount)
  noLoop();
}
