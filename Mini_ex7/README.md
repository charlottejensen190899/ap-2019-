![ScreenShot](54727849_790466921325981_7851070199838015488_n.png) 

URL: https://cdn.staticaly.com/gl/Nicolaibb/ap2019/raw/master/Miniex7/p5/empty-example/index.html


In the program, which have been created by Nicolai, Niels and I, we have worked
with autonomy and generativity through the making of simple rules. Like generative 
art our program is made for the purpose of exploring new creative expression 
based on computation. Our program is meant to be an expressive attempt express 
how surveillance has spread worldwide. In the process of making our program 
autonomous, we searched for the webpage http://www.generative-gestaltung.de/2/ 
and found the sketch called  P_2_2_4_01, which we used as a departure point for 
our program. Like the sketch P_2_2_4_01 I believe that our program can be 
categorized as generative art. According to Philip Galanter generative art “[...]
refers to any practice where the artist uses a system, such as a set of natural
language rules, a computer program, a machine or other procedural invention, 
which is set into motion with some degree of autonomy contributing to or 
resulting in a completed work of art”. In our work with our program we have used 
a set of natural languge rules, which we have come up with, and a computer 
program, which can set our progra in motion. With these two systems in place we
can create a program that to some degree can create a work on its own.
In the speech “Beautiful rules: generative models of creativity” performed by 
Marius Waltz most artists within the field of generative art work with some basic
principles, including a creation out of nothing, formal rules, complexity arising 
from simple rules and dynamic systems with autonomous behavior. One of the basic
principles is the creation "ex nihilo", which means that the generative art is 
created out of nothing. In relation to our program one could argue that we have 
not created it out of nothing, because our program does not begin with a plain 
background like it is the case with most generative arts, including Langton's 
Ants. Instead we have chosen to create our program so that it seems like our 
generative art has been created from earth itself.By doing this we could emphasize
how surveillance has spread across the world. Another principle is the complexity
arising from simple rules, which can be seen in relation to our program. In our
program we have created seven simple rules, which over time can create a complex
pattern that could not be made with analoque tools.The seven rules, which 
serve as the basis of our program can be seen below: 
    
	Rule #1: Set the first rectangle in Africa. 
	Rule #2: Generate a random size, x position and y position for the new rectangle. 
	Rule #3: Check which rectangle is the closest to the current rectangle.   
	Rule #4: Show the original position of the rectangle in the shape of a circle and create a line to the new position that represent the angle. 
	Rule #5: Align the new position to the closest rectangle outline. 
	Rule #6: Give the rectangle a random color within the green spectrum. 
	Rule #7: Repeat until the max count of the rectangles is reached. 

These rules play a role in determining how the program will evolve over time, 
which depending on the rules can cause an effect on the overall expression of 
the program. In the program one will see that the rectangles will begin at one 
particular place every time the program is refreshed, and from that place branch
out in different direction forming ramifications that will spread worldwide. We 
have chosen to make the point of departure in our program within the continent 
of Africa- more specifically in Ethiopia. The reason for this is not because 
surveillance came from this country. It was more a matter of how to show that 
surveillance as a phenomenon has evolved from one place, which eventually has 
spread worldwide.In comparison to Langton's Ant we have created a program, which 
tries to describe a phenomenon in the world. Unlike Langton's Ant we have not tried
to capture the kinds of emergent collective behaviors in colonies of social i
nsects in which individuals alter their environments which in turn causes them 
to alter their environments under the right circumstances. Instead we try to 
capture how an abstract phenomenon like surveillance possiblily could have 
evolved. What kinds of rules should be made, depends on what one wants to show 
with the program and what expression one wants to achieve. 

One syntax, which has have an impact on how our program evolves, is the random 
syntax. In our program we have used the random syntax on several occasions to give
the computer some control of the outcome, which makes the program seem more 
autonomous. Whether a program is autonomous or show a autonomous behavior or not
can be discussed. However, I believe that if a program needs to be told what to 
do and cannot make a decision that goes beyond this, the program can only be 
considered a co-author. One of the only thing that the program can decide on is 
the source code that involves the random syntax. An example of this can be seen 
in the creation of a random set of parameters for the rectangles, which can be 
seen in the following source code,    

	// create a random set of parameters
 	 var newR = random(3, 7);
 	 var newX = random(newR, width - newR);
 	 var newY = random(newR, height - newR);
 	 
In this excerpt of the source code we have used random with to parameters, 
meaning that it will return a random number from the first argument up to, but 
not including the second argument. If we look at the variable newR, the number 
that will be returned will be between three and seven where the number three is 
included and the number seven is excluded. One could say that the use of random 
in computational contexts does not represent pure randomness, because each number
is a function of the one before it. In that way randomness will in some ways 
always be predetermined within the computer. The random syntax we have used in 
our program, will likewise never be randomness in its pureste form, but instead
only appear to be it. The type of randomness that only appears to be random is 
referred to as pseudo-randomness.  

Working with this mini exercise I have gained an understanding of how generativity 
and automatism can be generated in a program through the use of simple rules and
the random function within the p5.js library. 