![ScreenShot](57294227_2049070618729136_6261526322829328384_n.png)

Link to flowchart: https://frafud.axshare.com/#g=1&p=home 

I have chosen to decompose my program from [mini exercise 6](https://gitlab.com/charlottejensen190899/ap-2019-/tree/master/mini_ex6%20kopi ) 
into definable parts, which I then used to construct a flowchart. In the beginning
I had some difficulties in deciding how technical I wanted my flowchart to be and
to whom I wanted to construct it. In the end, I decided to create a flowchart that
which centered around the technical details of the game, while still trying to make
it understandable for a novice programmer. In order to create such a flowchart I 
went through every line of code in my program to get an understanding of what the
underlying structure and hierarchy were before representing it in a flowchart. What
I found the most challenging in this process, was to figure out what the right
balance between complexity and simplicity was and how much each of them should weight
in the flowchart. I constructed my flowchart in a relatively complex way to show 
which technical elements were built into the program and how they were related. 
The idea was that the language I used in my flowchart should make much easier for
those reading the flowchart to follow how the program was constructed and how it 
worked despite the its complexity. 

![ScreenShot](57352926_334426947216757_3650420815154905088_n.png)

Link to flowchart: https://ihhyfx.axshare.com/#g=1&p=flowchart__1 

When brainstorming for the final project we came up with two ideas we wanted to 
construct as flowcharts. For the first idea we wanted to create a game that 
confronted the player with dilemmas, which he/she had to consider before 
he/she was able to continue playing the game. The way in which we imagined the 
player could be confronted with these dilemmas was through three doors. The 
thought was that when the player through his/her character chose a door, the 
program would provide the player with a random dilemma. One of the he technical
challenges for this idea might be how to use object oriented programming to make
an abstraction of the character and the doors in the program. Tackling this
challenge will require us to decide which attributes and behaviors we want to 
include and exclude in the abstractions of these. Another technical challenge for
this idea might be how to store and select the dilemmas.To tackle this we have 
talked about storing the dilemmas in an external JSON file, in which we would use 
the random function to select a dilemma to be displayed. Because of the many 
technical considerations and challenges in this idea, we chose to construct a 
flowchart that represented the user's way through the game, which we thought 
contributed to the best understanding of how the game works. 


![ScreenShot](57420708_643774762735409_273057923142254592_n.png)

Link to flowchart: https://3vgl72.axshare.com/#g=1&p=home 

For the second idea we wanted to create a program that could capture the user's 
feelings through a webcam and display it as an emoji on the canvas. If the user 
express any of the pre-programmed feeling which include 'happy', 'angry', 'sad',
'disappointed' and 'surprised', the program will display an emoji that matches the
emotion on the user's face. One of the technical challenges for this idea might 
be how to track and determine the user's emotion through a webcam. Another thing
we might consider is how we want to display and represent the captured feelings 
in the shape of emojis. To tackle this in a more technical manner we have to 
consider which colors and shapes that constitute a face and how this might be
composed to express the chosen feelings. In this idea we chose to make a more 
technical flowchart to show how the program works, because the idea was crafted 
from a more technical point of view. 
 
All flowcharts I have presented in this readme-file show different ways of using
a flowchart. The mini-ex 6 flowchart differs from the others by being constructed
after the game has been programmed, which means that I was able to go more into 
details with the technical aspect of the game and thereby making it seem more 
finished. The flowcharts created in collaboration, however, served more as drafts
to how our final project was supposed to work. For that reason these flowcharts 
are less technical complex and built in a more simplictic way than my own flowchart.
Even though these flowcharts both serve as drafts, the way in which they represent
the idea differs. Whereas we in the first idea sees the game from the user's point 
of view, the second idea is more centered around the programmer's view on how the
program should work technically. This suggests that to whom the flowchart is 
intended, has an impact on how the idea is represented in a flowchart. One could 
say that flowcharts are a useful way of showing solutions to whole problems even
when they are complex. 

According to the YouTube video 'Algorithms in pseudocode and flow diagrams' by 
Cambridge GCSE Computing Online a flowchart is just one way of representing 
algorithms visually. Another way to represent algorithms is by writing pseudocode,
which serve as a useful way of communicating how a problem solution might look in
code. An algorithm cannot be described as just one entity but as multiply entities.
When people speak of algorithm they often think of different descriptions of what
algorithms are and what they can do varying from accounts of computer scientist to
media users. According to Tania Bucher algorithms can be seen as both "magical and
concrete, good and bad, technical and social". What is meant by this is that 
algorithms exist on many scales ranging from how software operates on a technical
level to its impact on society at large. According to matematician professor Marcus
du Sauloy algorithms has taken over the modern life without us noticing it. 
Algorithms run everything around us from search engines on the internet to credit
card data security. It is said that they even help us travel the world, find love 
and save lives. This could indicate that these algorithms in secret have set up 
rules for our lives that we do not think about. To most ordinary people these
algorithms might be seen as blackboxes where the content of these are unknown.   



