let button;
let colortype=[240,211,182,153,124,95,66,37,8];
let word=["NI","WIKIPEDIA","PODRÍA","DEFINIR","LO","QUE","SIENTO","POR","TI"];


function setup() {
  createCanvas(600, 500);
  background(200) //background set to white

//element #1: start screen
  //white start screen
  push();
  fill(255);
  rect(width/3.2,height/8,230,380,25);
  noStroke();
  // red and green circle
  fill(255,0,0);
  ellipse(240,360,40)
  fill(0,255,0);
  ellipse(365,360,40)
  stroke(0);
  pop();

  // text on start screen
  textFont('andale mono');
  textSize(10);
  text('Call from your boyfriend',230,110)
  text('mobile',230,125);

  //icons for accepting and declining a call (the small phones)
  push();
  noStroke();
  fill(255);
  rect(229,355,20,5,40);
  rect(227,357,6,8,20);
  rect(246,357,6,8,20);
  rect(359,352,5,17,40);
  rect(361,350,9,6,40);
  rect(361,366,9,6,40);
  pop();

//element #2: the button
  // Phubbing button (the annoying habit of ignoring someone for your cell phone )
  button=createButton('Phubbing');
  button.position(width/2.25,419);
}

function draw() {
  button.mousePressed(textcolor)

// element #3:the black mobile phone
  //mobile phone
  push();
  noFill();
  strokeWeight(24);
  rect(width/3.2,height/8,230,380,25);
  fill(0);
  rect(width/3.2,height/1.2,230,2.2,25);
  strokeWeight(1);
  fill(200);
  rect(width/2.18,59,50,4,10);
  pop();

}

function textcolor (){

// element #4: the fading text
  textSize(25);
  textFont('apple symbols'); //font set to apple symbols
  textStyle(NORMAL); //style set to normal
  textAlign(CENTER); //alignment set to center
  textLeading(20); //leading set to 30

// a for loop that changes the alpha value and y-position for every word in the array
let ypos2=160;
var x=0;
    for(var i=0; i<word.length; i++){
        text(word[i],300,ypos2);
        fill(0,colortype[i]);

        ypos2=ypos2+25;

      }

  }
