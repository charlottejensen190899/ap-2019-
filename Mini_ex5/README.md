![ScreenShot](53294707_356785488248610_8354548024579457024_n.png)

![ScreenShot](53711590_2058180061141708_8148885190647742464_n.png)

URL: 
https://cdn.staticaly.com/gl/charlottejensen190899/ap-2019-/raw/master/Mini_ex5/empty-example/index.html 

When I thought of which of my mini-ex'es I wanted to revisit for my mini-ex5, I 
thought of the first program I made in aesthetic programming. When looking back 
at my mini-ex1 and reading through my README-file I became aware of how much 
knowledge about syntaxes, I had gained through studing different syntaxes with
the aim of understanding these. 

In my mini-ex5 I wanted to use the gained knowledge and create a simpler and 
more efficient source code than I did in my mini-ex1, but with more or less the
same expression. I  chose to pursue with my original idea from  mini-ex1, which 
can be seen in the screenshot below, and make the expression of this idea of 
sweet words fading into the background even clearer. 

![ScreenShot](54233774_274209986825857_4147167756128616448_n.png)

It is stated in the article "the practices of programming" by 
Ilias Bergström and Alan F. Blackwell that there in programming are different 
types of practices and reasons for writing programs. In the case with my mini-ex1 
I was programming to figure out the different syntaxes and exploring the 
materiality of programming. Bergström and Blackwell would categorize this 
programming practice into the category bricolage and tinkering, which is an 
ideal practice for someone who is unsure about the possibilities and limitations 
of programming and is in the early process of learning to program. However, in 
my mini-ex5 I went to the assignment with another practice of programming, which
was slightly different from the bricolaging and tinkerng practice I had in 
mini-ex1 in terms of how I used the code and what purpose it was used for. 
Instead of just exploring the syntaxes and learning to read and write these in a
program, as Annie Vee states that everyone should learn, I used programming as a
tool to sketch out different variations of my idea. This type of practice 
Bergström and Blackwell would categorize as sketching with code. I would say 
that my mini-ex1 served as a way for me to try out things and see what happened,
whereas my mini-ex5 more served as a way to explore different ideas within the 
expression that I wanted the program to have. The practices in programming can 
in my opinion serve as different lenses to look upon programming and work with 
programming.

Conceptually, I wanted to bring my idea into a social context and thereby 
emphasize how our mobile phone has destroyed our relationships with other 
humans, because of our habit to neglect human relationship in favor of the
mobile phone. This phenomenon is also known as "phubbing". The word "phubbing"
is a contraction of the words "phone" and "snubbing" and is often used in 
occasions where a person has more focus on his or her phone than the person 
or persons they are together with.The way I tried to capture this at a technical
level was to create different elements that in symbiosis should constitute to the 
overall expression. The different elements in the prgram was 1) the start screen,
2) the button, 3) the mobile phone and 4) the fading texts. 

The start screen and the mobile phone element were made through the use of the 
syntaxes in the categories of shapes, colors and typography and had the purpose
of establishing a context in which the user is confronted with the choice of 
"phubbing". I chose to create a context around the use of a mobile phone, 
especially the act of either accepting or declining  a phone call. Instead of 
just giving the user the ability to accept or decline a phone call, I decided
to give the user the possibility to take an active choice of phubbing someone 
through a push on a button. In this way the user would be forced to actually 
chosing to neclect a human relationship in favor of the mobile phone and not 
just doing it out of the user's habit. I created the "phubbing" button through
the DOM-library and attached the attributes of the function(textcolor) to the 
button when pressed. I wanted to display the fading text element insde the 
function(textcolor), because I think it would create a stronger message about 
the effects of phubbing in society, if the user had to press the button first 
and then see the fading words on the start screen. In contrast to how I made the 
fading words in my mini-ex1, I made use of arrays to make the code more
efficient by gathering the text and the alpha values inside an array. Besides 
that I used a for loop to change the alpha- values and the y-position for every 
word in the array called word. In this way I could create more or less the same 
expression as in my mini-ex1.

Through the work of this mini exercise I believe that programming can be used 
as a tool to emphasize and materialize tendencies like phubbing in our digital
culture that we may not be aware of. In a way one could say that programming
can contribute to the discussion of what effect technology has on the society 
today and perhaps create an awareness of these effects.