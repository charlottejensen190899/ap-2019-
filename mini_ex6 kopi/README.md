URL: https://cdn.staticaly.com/gl/charlottejensen190899/ap-2019-/raw/master/mini_ex6%20kopi/empty-example/index.html 

![ScreenShot](54515940_2041886086110429_4228615295431868416_n.png)

I have created a game where the goal is to collect as much money on one’s travel 
card as possible. The way one can get money on the travel card is by clicking on
the cars that run over the screen in random patterns. In my game I have tried to
make the rules as simple as possible so no further instructions are needed. The 
only instructions that appears in the program is, “get money on your travel card
by clicking on the cars.” I chose not to include what happens if one do not click
on the car for two reasons. The first reason is because I think that one 
relatively quickly discovers what happens if one misses a car. The second reason
is because it might give one a increased desire to collect more money as a result
of lossing money - like in the case with people gambling at casinos. The rules 
for my game is if the cars are being clicked on, one gets money on the travel 
card and if not, one will lose money. The inspiration for this game came from 
the functionality of the travel card and the concept of gambling as something one
can win or lose on and which can continue for a long period of time. Especially 
the way I have created my point system has gambling as its starting point. This 
is partly due to the fact that the game generates changes of gaining money and 
risks of lossing money and partly due to the way the game invites the player to 
continue playing because there is not a sign of either winning or lossing the game. 
One could say that it is a never ending game. 

In my game I have used object oriented programming as my approach and in doing so 
I, as the programmer of the game, could decide what constituted a car and how 
this object should behave in the game. The first thing I made in my game was the
template to my car, which is called a "class" in object orientated programming. 
A class is a category of objects, which defines all the common properties of the
different objects that belong to it. Within a class one could create a form of 
recipe for how an object instance -a car in my case- looks and how it behave and
thereby achieve a block of code that could be re-used in the creation of new 
object intances in the program. In object oriented programming an object is a 
self-contained entity that consists of both data and procedures to manipulate the
data. In my game only had to create one class from where I could use the 
information to create a series of cars that run over the screen in random 
patterns. However, before the cars can be a part of the game, I had to pick out
common features of the car and its procedures, which is called abstraction in 
object oriented programming. I made this abstraction of a car by defining what
attributes and methods the object should be assigned inside the class. I chose 
to define a car by its x-position, its y-position, its speed and its size. To 
initialize the car object, I used the constructor method with the above mentioned
attributes as parametres. This can be seen in the following source code,

>
> class Car { 
>
>     constructor(speed, xpos, ypos, size) { 
> 
>     this.speed = speed;
>
>     this.pos = new createVector(xpos, ypos);
>     
>     this.size =size;
>     
>     }
>

The notion "this", which can be seen in the source code above, is used to refer
to a particular attribute such as the position, speed or size of an object. 
The reason for this notion, I think, is to create a universal template from which
the programmer can create several objects and not just one object. As Matthew 
Fuller and Andrew Goffrey writes in the article: The Obscure Objects of Object 
Orientation "a computational object is a *partial* object: the properties and 
methods that define it are necessarilly selective and creative abstraction of 
particular capacities form the thing it models, and which specify the ways that 
it can be interacted with"(p. 6). By this they mean that objects - in my case 
the car - is created by a programmer, who has chosen which properties/attributes 
and methods that should define the object and thereby created an abstraction of 
what a real car is and what it constitutes. In this way you could say that the 
programmer has the power to change how we perceive a object and the environment 
round it. The same is the case with the methods of the object. Like the attributes
I have chosen, the method too represent how I have defined the object's possible 
interactions with its environment. One example of how I have programmed a method 
can be seen in the following source code: 

>
> move() {
>
>     this.pos.x+=this.speed;
>
>     if (this.pos.x > width+1) {
>
>       this.pos.x = 0;
>
>     }
>

In this method I have chosen to give the car a behavior in the form of a movement
from one side of the screen to another, which continues over and over again in a
never ending cirle. With this method I define a car as something that can move 
from one place to another. I could have chosen to create more or other methods to
get closer to what a car is in the real world. However, I would never be able to 
encapsulate a car that captures all areas of what a car is and what funtionalities
it has. The process of encapsulation is a process of combining elements to create
a new entity. In my program I combine the attributes and the methods defined in
the class to create an encapsulation of what a car is and how it behave. The 
information that is hidden inside a class can serve as a powerful programming
technique because it reduces complexity in the game. By reducing the complexity 
of a game it is easier to keep track of what the different element does in the 
program. For example in my game I use the class as something I can refer to in 
other blocks of code to create a more readable code. To use the class, I have 
created, I made a variable in the form of an empty array in which I could store 
variables related to the car's attributes and methods. To put the data into the 
empty array I used a push function inside a function I called addCar, which can 
be seen in the source code beneath, 

>
>function addCar(){
>
>  car.push(new Car(floor(random(1,15)), floor(random(0,20)), floor(random(height_limit-100,height_limit+110)), floor(random(20,80))));
>
>}
>

This function has the purpose of creating new car objects with a random value of 
the vairables speed, x-position, y-position, and size within a particular range. 
Besides this function I made a for loop in the draw function to call the methods 
show and move from the class. Within the for loop I asked the computer to go 
through the array called car and checking if the length of the array is less than
the variable i and if so increment the variable with one, which can be seen in 
the source code, 

>
>for (let i = 0; i <car.length; i++) {
>
>    car[i].move();
>
>    car[i].show();
>
> }   
> 

What this mini-exercise has taught me is that one's programs always has an effect 
on how we perceive the objects in the program. One will never be able to capture
all aspects of an object and is therefore forced to chose what attributes and
methods that constitute the particular object. In the end it is the programmer 
that has the responsibility for how an object is defined- whether it is a car or
a human being that is being defined in a program does not matter. What is important 
is that the programmer is aware of the constraints and implications of creating
a class and an object, which is inherent in object oriented programming. 