let car = [];// declare object
let min_car = 10;
let height_limit;
let score=0;

function setup() {
  createCanvas(windowWidth, windowHeight);
  height_limit = height/4; //a reference point for the road and cars

  randomSeed(190);
  for (let i=0; i<=min_car; i++) {
    car[i] = new Car(floor(random(1,18)), 0, floor(random(height_limit-100,height_limit+110)), floor(random(20,80))); //create/construct a new object instance
  }
}

function draw() {
  background(255);
  drawRoad();
  for (let i = 0; i <car.length; i++) {
    car[i].move();
    car[i].show();
}

// creating the title of the game and the instructions on what to do in the program
fill(50); //fill on text set to black
textSize(30); // size of text set to 30
text("HEAVY TRAFIC",width/2,50); // title of the game
textSize(16); // size of text set to 20
text("Get money on your travel card by clicking on the cars",width/2,75); //instructions

//score
textSize(20);
text("Money collected: $"+score,width/2,height/1.5);

  // travel card that follows the mouse
  push();
  fill(255);
  stroke(0);
  rect(mouseX,mouseY,100,60,5);
  noStroke();
  fill(0,0,255);
  arc(mouseX+4,mouseY+30,50,50,80,8,CHORD);
  textSize(9);
  text("Travel card", mouseX+40,mouseY+15)
  pop();
}

function drawRoad() {
  push();
  stroke(196,198,116,15);
  fill(19,100);
  rect(0,height_limit, width, height/2.95);
  pop();
}

function addCar(){
  car.push(new Car(floor(random(1,15)), floor(random(0,20)), floor(random(height_limit-100,height_limit+110)), floor(random(20,80))));
}

function mouseClicked(){
  var onCar = false;
  for (let i = 0; i <car.length; i++) {
  if(mouseX > (35*2)+car[i].pos.x && mouseX < ((35*2)+car[i].pos.x)+110 && mouseY >(38*2)+car[i].pos.y && mouseY<(38*2+car[i].pos.y)+60) {
      score++;
      console.log("score");
      onCar = true;
  }
}
if (onCar == false) {
  score--;
}
}

class Car { //create a class: template/blueprint of objects with properties and behaviors
    constructor(speed, xpos, ypos, size) { //initalize the objects
    this.speed = speed;
    this.pos = new createVector(xpos, ypos);
    this.size =size;
    }

  move() {  //moving behaviors
    this.pos.x+=this.speed;
    if (this.pos.x > width+1) {
      this.pos.x = 0;
    }

  }
  show() { //show car
      push();
      fill(0,0,255);
      beginShape();
      vertex(90*2+this.pos.x,60*2+this.pos.y);
      vertex(90*2+this.pos.x,50*2+this.pos.y);
      vertex(80*2+this.pos.x,49*2+this.pos.y);
      vertex(70*2+this.pos.x,38*2+this.pos.y);
      vertex(48*2+this.pos.x,38*2+this.pos.y);
      vertex(35*2+this.pos.x,46*2+this.pos.y);
      vertex(35*2+this.pos.x,60*2+this.pos.y);
      endShape(CLOSE);
      fill(255);
      stroke(0);
      strokeWeight(6);
      ellipse(this.pos.x+95,this.pos.y+120,20,20);
      ellipse(this.pos.x+155,this.pos.y+120,20,20);
      //strokeWeight(2);
      //rect (35*2+this.pos.x,38*2+this.pos.y,110, 60) // used to create an area on which the car can be clicked in order to get money
      pop();
}
}
