![ScreenShot](52590100_304787156874895_1798354496766607360_n.png)



![ScreenShot](52859665_818016401870258_8262197079435116544_n.png)



URL: 
cdn.staticaly.com/gl/charlottejensen190899/ap-2019-/raw/master/mini_ex3/empty-example/index.html  

**Describe about your throbber design, both conceptually and technically**
When I thought of what a throbber is  and how it typically is designed, I thought of
the traditional throbber that appears on many websites when performing a blocking action.
As users we usually encounter a throbber during the loading, waiting or streaming of data
content. A throbber usually appears in a graphical format that sometimes is has the word
“loading” attached to it. In my design of a throbber I took the traditional throbber as a
source of inspiration. 

Technically, my source code had roots in Winnie’s sketch of a yellow throbber consisting 
of what appears to be nine ellipses. In the beginning of my process I copied Winnie’s code
and tried to modify some of the parameters to see what happened. Instead of having the 
ellipse appear in nine positions, I chose to have it appear in twelve positions to imitate 
a clock.To proceed further with the association to a clock I chose to have four ellipses 
moving around at different distances from the center. The idea was that these moving ellipses
should constitute the clock’s display. I chose to have a relatively low frame rate to have 
the ellipses imitate a clock ticking. At a technical level I changed the number of frames 
to be displayed every second. I called the function frameRate(1), which meant that the 
program would attempt to refresh one time a second. A frame rate on one gives the program
a rough feeling to the animation, which in my case was on done on purpose to make the user 
become more aware of the waisted time and the inherent frustration. In this manner I tried
to give the throbber a more real time perception than the traditional throbber, which is 
entangled with computational logics. 

The fact that the throbber only indicates that something is loading and in-progress, 
changes the user’s perception of time because it represents an unstable stream of the now 
for an indeterminate and unforeseeable timespan.To emphasizes the user’s perception of time
when a throbber appears I chose to incorporate an additional element into my throbber, which
was the words ‘now’ and ‘wait’. I chose to arrange the words in a way so that imitates a 
clock in order to highligth the word within the 'display' of the clock. The word ‘now’ should
represent the stream of the now that the user experiences when seeing a throbber appear, while 
the word ‘wait’ should contribute to the indeterminate and unforeseeable timespan of the 
throbber.Together these words should create an awareness of the user’s perception of time. 

The conceptual idea behind the program was to create a program that takes up the issue of time, 
especially the gap between the user’s exception of time and the computational clock that 
drives the micro-temporality of instructions inside the computer. In order to take this issue
up I made a throbber that would create connotations to both an eye and a clock. With the 
connotations of an eye, the program could be understood as the materialization of the user’s
perception of time where the words operate as the user’s thought of the experience with athrobber.
With the connotations of the clock, the program could be understood as a materialization of real 
time in the shape of a clock but with the functions happening in computational time. This 
connotation could potentially make the gap between real time and computational time seem smaller. 



