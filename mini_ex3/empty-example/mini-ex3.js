
var words =['Now','Wait']
  function setup(){
  createCanvas(400, 400);

}

function draw() {
    background(255);
    fill(220);
    noStroke();
    ellipse(200,200,190,190);
    fill(190);
    ellipse(200,200,90,90);
    frameRate(1);
    drawThrobber(12);
    fill(220);
    textSize(12);
    text(words[1],187,130); //kl.12
    text(words[1],187,277); //kl.6
    text(words[0],150,268); //kl.7
    text(words[0],122,240); //kl.8
    text(words[1],115,205); //kl.9
    text(words[0],125,167); //kl.10
    text(words[0],152,140); //kl.11
    text(words[0],222,267); //kl.5
    text(words[1],259,205); //kl.3
    text(words[0],250,240); //kl.4
    text(words[0],250,170); //kl.2
    text(words[0],225,140); //kl.1

}

function drawThrobber(num) {
  push();
  translate(200, 200); //move things to the center
  // 360/num >> degree of each ellipse' move ;frameCount%num >> get the remainder that indicates the movement of the ellipse
  let cir = 360/num*(frameCount%num);  //to know which one among 9 possible positions.
  rotate(radians(cir));
  noStroke();
  fill(255); //set fil to white
  ellipse(73,0,40,40);  //the moving dot(s), the x is the distance from the center
  ellipse(40,0,20,20);  //the moving dot(s), the x is the distance from the center
  ellipse(17,0,15,15);  //the moving dot(s), the x is the distance from the center
  ellipse(1,0,10,10);   //the moving dot(s), the x is the distance from the center
  pop();
}
