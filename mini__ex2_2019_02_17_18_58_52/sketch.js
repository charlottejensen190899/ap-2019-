function setup() {
  createCanvas(500, 400);
  
}

function draw() {
  background(0);
// emoji #1
  if(mouseIsPressed){
  strokeWeight(1);//default 
  stroke(255,145,0); // set stroke to orange 
  fill(255,220,0);//set fill to yellow
  rect(24,130,36,39,30);
  rect(178,130,36,39,30);
  ellipse(120, 146, 145, 209);
    
  }else{
  strokeWeight(1);//default 
  stroke(255,145,0); // set stroke to orange 
  fill(255,220,0);//set fill to yellow
  rect(13,130,36,39,30);
  rect(190,130,36,39,30);
  ellipse(120, 146, 175, 175);
  
  }
  print(mouseIsPressed);
  
	// tears
  noStroke();
  fill(0,0,255); //set fill to blue
  rect(142,140,30,89);
  rect(62,140,30,89);
	// eyes
  stroke(0);
  strokeWeight(11.0);
  fill(0);
  line(140,140,169,140);
  line(63,140,93,140);
	//eyebrown 
  stroke(0,95);
  strokeWeight(6.9);
  fill(0);
  line(61,110,89,99);
  line(141,99,169,110);
	//mouth
  noStroke();
  rect(100,175,34,20,80);

//emoji #2
  if(mouseIsPressed){
  strokeWeight(1);//default 
  stroke(255,145,0); // set stroke to orange 
  fill(255,220,0);//set fill to yellow
 	rect(415,130,36,39,30); //ear
  rect(277,130,36,39,30); //ear
 	triangle(250, 70, 480, 70, 365, 276); //face shape #3
    
  }else{
  strokeWeight(1);//default 
  stroke(255,145,0); // set stroke to orange 
  fill(255,220,0);//set fill to yellow
  rect(427,130,36,39,30); //ear
  rect(262,130,36,39,30); //ear
  rect(286, 69, 155, 165, 20); //face shape #3 
  
  }
  print(mouseIsPressed);
  //eyes
  stroke(0);
  fill(255);
  ellipse(328,149,44,44);
  ellipse(400,149,44,44);
  fill(0);
  ellipse(333,139,15,15);
  ellipse(392,149,15,15);
  //eyebrowns
  stroke(0,95);
  strokeWeight(4.9);
  fill(0);
  line(399,110,417,120);
  line(309,120,330,110);
  //mouth
  textSize(41);
  text('💋',350,210);
	//text
  fill(255);
  textSize(20);
  textFont('microsoft sans serif');
  text('Introducing the new shape modifiers', 90, 300);
  textSize(14);
  text('Just press the mouse', 200,330);
}