![ScreenShot](52441748_571827669953369_5824127622203834368_n.png) 
![ScreenShot](52306206_305746270141567_346251424360824832_n.png)

URL:
https://cdn.staticaly.com/gl/charlottejensen190899/ap-2019-/raw/master/mini__ex2_2019_02_17_18_58_52/index.html


My program shows two emojis with the ability to change their shape when the mouse is pressed. 
In order to create this change in shape I used an if statement for both emojis. I, then, 
decided on which shapes should be displayed when the program started and which shapes should
be displayed when the mouse is pressed. I placed the shapes I wanted to display when the 
program started under the word ‘if’ in the if statement and I placed the shapes I wanted to 
display when the mouse was pressed under the word ‘else’ in the if statement. I could now 
change the shape of the emojis’ faces without changing the facial expression of the emoji.  

In my coding process I built upon the knowledge gained from mini_ex 1 and learned more about
two things. The first thing I learned about was the syntax of 2D shapes, especially the shapes
rect(), ellipse(), line(), and triangle() in the process of creating the two emojis. The shape
I found most difficult to use was the line. This is due to me struggling with the location of 
the eyebrows but eventually I figured it out. I would not say that the syntax of line() was
difficult to understand, for me it was more difficult to place the line the way I wanted it to
be. However, after changing the numbers multiply times, I succeeded in placing the eyebrows and
from that I gained some knowledge about how to use the line() in future work.    

The second thing I learned about was how to use an if statement that included the boolean system
variable mouseIsPressed. I found out that this variable only can have two answers either true if 
the mouse is pressed or false if it is not. In my program I have used this knowledge to change the
shapes of my emojis. I chose that if the variable was false (if the mouse was not pressed) the 
shapes ellipse and square was shown in the program. If the variable instead was true (if the mouse
was pressed), an oval ellipse and a triangle will be shown in the program. 

These shapes that I used in the if statement should represent different types of face shapes
-though in a caricatured version.The idea was to break the traditional emojis’ round shape 
and give the user the possibility to choose a face shape that matches the user’s face shape- 
just like the skin modifiers. To follow the tendency that is described in the text, modifying 
the universal I chose to take it to the next level by giving the emojis ears and different shapes
in order to make them more human and mirror the user even more. In this way emojis could be an 
identity statement for the user instead of just portraying the user’s feelings. This, however, 
goes against what is expressed in modifying the universal, where it is states that it does not 
make sense to fix political issues by finding a less controversial standard for expressing skin 
tone, or to solve the problem by adding yet more variables to an emoji. 

Even though I try to solve the problem by adding more variables to the emojis I still think that
my emojis could take part in the discussion on whether or not emojis should represent the user’s
identity and have more human characteristics. They serve as an example of how the problem could be 
fixed by adding more variables. However I agree with what is stated in the text. The political 
issues should not be converted into a technical problem, because it does not fix the real problem.   

However, the emojis that I had designed could be put into a wider cultural context that concerns 
identification and identity. The reason for this is that these emojis take the issue a step further 
by adding more human characteristics, which adds to the identification with the emojis and to the 
use of emojis as representations of ones identity. Perhaps this is the next thing we will be 
introduced to by the Unicode Corsortium 

 
