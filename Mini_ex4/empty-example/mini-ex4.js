
let radio;

function setup() {
  createCanvas(780, 400);

    radio = createRadio();
    radio.option('Authenticity', random(100));
    radio.option('Beauty', random(100));
    radio.option('Compassion', random(100));
    radio.option('Determination',random(100));
    radio.option('Fame',random(100));
    radio.option('Happiness',random(100));
    radio.option('Kindness',random(100));
    radio.option('Love',random(100));
    radio.option('Plesure',random(100));
    radio.option('Wealth',random(100));
    radio.style('width', '960px');
    textAlign(CENTER);
}

function draw() {
  background(240);

  if(mouseX<width && mouseY<height)
    cursor(HAND)

  var val = radio.value();
    if (val) {
      textSize(20);
      text('Your value is assigned to $' + val, width /2, height /2);
    }

//creating the bar code
randomSeed(random(255));
for (let i = 0; i < 100; i++) {
  frameRate(0.9);
  let r = random(0,355);
  stroke(r);
  line( i+340, 240, i+340,299);
 }


// text: Declare your value
//(inspiration from working with variables in programming)
  noStroke();
  textSize(40);
	text('Declare your value! ',width/2,height/3);

  textSize(12);
  text('Choose one of the values below:',90,396);

}
