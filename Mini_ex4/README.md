![ScreenShot](53533229_391625394734194_5619619352790695936_n.png)

URL: https://cdn.staticaly.com/gl/charlottejensen190899/ap-2019-/raw/master/Mini_ex4/empty-example/index.html 

In my sketch I have chosen to work with the concept of human values, because I 
wanted to challenge the view of what these values are worth in society. Some may
say that values like love, happiness and authenticity are immeasurable and cannot
be captured in a computer program.

To capture these human values in a computer program, I had to assign them a 
number to make the program work, because computers cannot handle immeasurable 
values like we humans can. So in order to capture and handle human values I had 
to make them more measurable.Technically, I created a variable called "var”, 
which I assigned to the value of radio.value( ); to give the human values a value
in the program (see the source code, line 28-31). I chose to assign these values 
with a value in USD, because I think that the viewer has a clear sense of what 
this valuta is worth.

To add something extra to the viewer’s thoughts on what objects or concepts are 
worth, I created a bar code to give the viewer an association to buying something 
e.g. in a store. I chose the bar code because I think it gives a visual 
representation of some information that can be captured by a computer when being 
scanned.

In my sketch I tired to capture the information about human values in the form 
of radio buttons. I labelled each radio button with a human value and assigned 
each of the human values with a random number from zero to hundred as you can 
see in the source code beneath,

>         radio = createRadio();
>
>                radio.option('Authenticity', random(100));
>
>                radio.option('Beauty', random(100));
>
>                radio.option('Compassion', random(100));
>
>                radio.option('Determination',random(100));
>
>                radio.option('Fame',random(100));
>
>                radio.option('Happiness',random(100));
>
>                radio.option('Kindness',random(100));
>
>                radio.option('Love',random(100));
>
>                radio.option('Plesure',random(100));
>
>                radio.option('Wealth',random(100));


However, by choosing these radio buttons instead of normal buttons in my 
program I constrain the viewer to only choosing one of the human values and 
then reduce the concept of human values even further. According to Søren Pold
a button indicates a functional control where something well defined and 
predictable will happen as a result of the user pressing it. The same is the
case with my radio buttons in the sketch. In the source code above you can see 
that I have labelled my buttons with different human values and have given them
a random value from zero to one hundred. So when one of the buttons
is pressed, by the viewer, the outcome of the program will always be predictable
and well defined, because it will always pick a random number from zero to one
hundred when the buttons are pressed no matter what button is being pressed. 
In ths way the buttons in the sketch establish a functional control over the
outcome of the program, which always is predictable and well defined. 
Furthermore these buttons are used as a way of expressing my understanding of 
how computers work as a machine that needs to process values in a differnt way 
than humans process values like love, happiness and authenticity in society. 

My inspiration for the text, which is displayed in the sketch, came from 
working with variables in programming. Like working with variables in 
programming, the viewer has to declare a variable in form of human
values in my sketch. However, these variables have been chosen in advance, 
which contribute to a definition of what human values are and what these values 
are worth in the program. To give concepts that humans sees as immeasurable a 
value in the form of a number, it creates a reduction in the concept of human
values, making them seem more inhuman or computational in a way.

By working on this mini_ex I became aware of the way that machine values differ
from human values and how difficult it is to capture human values in the computer
without reducing the human values to only numbers on a screen. 


 
